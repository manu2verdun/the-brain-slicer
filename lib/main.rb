require 'titleize'

brainfile = open("brain/thebrain.in", "r:UTF-8")
line_num = 0
file_num = 0
artist_idx =0 
title_idx = 0



brainlines = Array.new
braincue = Array.new

brainfile.each_line do |line|
  brainlines << line
end

braincue << "REM GENRE \"The Brain\""
braincue << "REM ALBUM ARTIST \"#{brainlines[1][0..brainlines[1].size-2]}\""
braincue << "TITLE \"#{brainlines[2][0..brainlines[2].size-2]}\""
braincue << "FILE \"#{brainlines[3][0..brainlines[3].size-2]}\" MP3"

(4..brainlines.size-1).each do |line_number|
  artist_idx = brainlines[line_number].index('-')
  title_idx = brainlines[line_number].index('-', artist_idx+1)
  braincue << " TRACK #{file_num +=1} AUDIO"
  braincue << "   TITLE \"#{brainlines[line_number][artist_idx+2..title_idx-2]}\""
  performer = brainlines[line_number][6..artist_idx-2]
  perfomerupper = performer.upcase
  if performer = performer.upcase
    performer = performer.titleize
  end
  braincue << "   PERFORMER \"#{performer}\""
  braincue << "   INDEX 01 #{brainlines[line_number][0..4]}:00"
end


braincuefile = open("D:/Downloads/thebrain.cue", 'w:UTF-8')

braincue.each do |line|
  puts "#{line}"
  braincuefile.write(line)
  braincuefile.write("\n")
end

brainfile.close
braincuefile.close

